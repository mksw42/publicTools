# Install gitlab

```bash
helm repo add gitlab https://charts.gitlab.io/
helm repo update

helm upgrade -i gitlab gitlab/gitlab -n gitlab \
  --timeout 600s \
  --set global.hosts.domain=domain.com \
  --set postgresql.image.tag=13.6.0 \
  --set certmanager.install=false \
  --set global.ingress.class=nginx \
  --set nginx-ingress.enabled=false \
  --set ertmanager-issuer.email=devops@mk \
  --set global.ingress.configureCertmanager=false \
  --set global.edition=ce
```

## helpful

```bash
kubectl create secret generic gitlab-gitlab-initial-root-password --from-literal=password="admin123" -n gitlab
kubectl patch svc gitlab-nginx-ingress-controller -p '{"spec": {"type": "NodePort"}}' -n gitlab
```
