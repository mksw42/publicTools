# Deploy Vault - K8s

```bash
k apply -f vault-k8s

kubectl exec vault-0 -- vault operator init -key-shares=1 -key-threshold=1 -format=json > keys.json

VAULT_UNSEAL_KEY=$(cat keys.json | jq -r ".unseal_keys_b64[]")

echo $VAULT_UNSEAL_KEY

VAULT_ROOT_KEY=$(cat keys.json | jq -r ".root_token")

echo $VAULT_ROOT_KEY

kubectl exec vault-0 -- vault operator unseal $VAULT_UNSEAL_KEY
kubectl exec vault-0 -- vault login $VAULT_ROOT_KEY
```
TOKEN is our password

```bash
kubectl exec vault-0 -n vault -- vault login $VAULT_ROOT_KEY
```
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Vault is on NodePort - nodeIP:32000
