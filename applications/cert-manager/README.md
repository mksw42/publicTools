# Certmanager

https://cert-manager.io/docs/installation/helm/

## How to install

```bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.crds.yaml
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.5.4
kubectl get pods --namespace cert-manager
```

# troubleshooting --- https://github.com/jetstack/cert-manager/discussions/3483

## How to create cert

```bash
k apply -f 01-issuer
k apply -f 02-create cert

openssl x509 -in <(kubectl -n sonarqube get secret \
  sonarqube-tls -o jsonpath='{.data.tls\.crt}' | base64 -d) \
  -text -noout
```

https://tech.paulcz.net/blog/creating-self-signed-certs-on-kubernetes

```bash
kubectl apply -n sonarqube -f <(echo "
apiVersion: cert-manager.io/v1alpha2
kind: Issuer
metadata:
  name: selfsigned-issuer
spec:
  selfSigned: {}
")
```
